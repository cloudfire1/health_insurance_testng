package com.healthInsurance.qa.Config;

import java.io.File;

public class Constants {

	public static final String CREDENTIALS_FILE_PATH = System.getProperty("user.dir")+File.separator
			+ "src\\test\\resources\\TestData\\credentials.json";

	public static final String TESTDATA_FILE_PATH = System.getProperty("user.dir")+File.separator
			+ "src\\test\\resources\\TestData\\TestData.json";

	
	
	public static final String WORKING_DIR = System.getProperty("user.dir");

	public static final String REPORT_DIR = WORKING_DIR + File.separator + "Reports" ;

	public static final String SCREENSHOTS_DIR = WORKING_DIR + File.separator + "src\\test\\resources" + File.separator
			+ "Screenshots" + File.separator;

	public static final String LOG4J_DIR_PATH = WORKING_DIR + File.separator
			+ "src\\main\\resources\\LOGS_CONFIG\\log4j.properties";

	public static final String PROJECT_NAME = "HEALTH_INSURANCE_DEMO";

	public static final String PROPERTY_FILE_PATH = WORKING_DIR+File.separator+ "src\\main\\java\\com\\healthInsurance\\qa\\Config\\Config.properties";

	public static final String DOWNLOAD_DIR = WORKING_DIR + File.separator + "DOWNLOADS" + File.separator;

	public static final long PAGELOAD_TIMEOUTS = 5;
	public static final long IMPLICITWAIT_TIMEOUTS = 3;

	public static final int KEY_STROKES = 3000;

	public static final int SYNC_TIMEOUT = 2000;
	public static final int MAX_TIMEOUT = 30;
	public static final int POLLING_TIMEOUT = 2;
}
