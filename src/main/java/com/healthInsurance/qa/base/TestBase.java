package com.healthInsurance.qa.base;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.Duration;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.ProfilesIni;
import org.openqa.selenium.support.events.EventFiringDecorator;

import com.healthInsurance.qa.Config.Constants;
import com.healthInsurance.qa.listeners.WebEventListener;
import com.healthInsurance.qa.util.Log;

import io.github.bonigarcia.wdm.WebDriverManager;

public class TestBase {

	public static WebDriver driver = null;

	public static Properties prop;

	public TestBase() {
		try {

			prop = new Properties();

			// FileInputStream fip = new FileInputStream(Constants.LOG4J_DIR_PATH);
			FileInputStream fip = new FileInputStream(Constants.PROPERTY_FILE_PATH);

			BufferedInputStream buf = new BufferedInputStream(fip);

			prop.load(buf);

		} catch (IOException e) {
			Log.error("IO EXCEPTION THROWN WHILE LOADING THE CONFIG FILE::::::" + e.getMessage());
		} catch (Exception e) {
			Log.error("GENERIC EXCEPTION THROWN WHILE LOADING THE CONFIG FILE::::::" + e.getMessage());
		}
	}

	public static void BrowserSetup() {
		String browserName = prop.getProperty("browser");

		if (browserName.equalsIgnoreCase("chrome")) {

			WebDriverManager.chromedriver().setup();

			ChromeOptions options = new ChromeOptions();

			options.addArguments("start-maximized");
			options.addArguments("--igcognito");
			options.addArguments("--disable-extensions");
			options.addArguments("--remote-allow-origins=*"); // selenium 4.8.0 dont use this property
			options.addArguments("disable-popup-blocking");
			options.addArguments("ignore-certificate-errors");

			driver = new ChromeDriver(options);

		} else if (browserName.equalsIgnoreCase("FF")) {

			WebDriverManager.firefoxdriver().setup();

			String downloadFilePath = Constants.DOWNLOAD_DIR;

			ProfilesIni profile = new ProfilesIni();
			FirefoxProfile testProfile = profile.getProfile("Automation_Testing");

			testProfile.setPreference("browser.download.dir", downloadFilePath);

			FirefoxOptions options = new FirefoxOptions();

			options.setProfile(testProfile);

			driver = new FirefoxDriver(options);

		} else if (browserName.equalsIgnoreCase("EDGE")) {
			WebDriverManager.edgedriver().setup();

			EdgeOptions options = new EdgeOptions();
			options.setCapability("window-size", "1920*900");

			options.setCapability("ignore-certificate-errors", true);

			driver = new EdgeDriver(options);

		} else {
			Log.error("THESE GIVEN OPTION IS INVALID DUE TO INVALID BROWSER NAME");
			throw new RuntimeException("Invalid Browser");
		}

		WebEventListener eventListener = new WebEventListener();

		WebDriver decorator = new EventFiringDecorator<WebDriver>(eventListener).decorate(driver);

		driver = decorator;

		driver.manage().deleteAllCookies();

		driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(Constants.PAGELOAD_TIMEOUTS));

		// driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(Constants.IMPLICITWAIT_TIMEOUTS));

		driver.navigate().to(prop.getProperty("url"));

		Log.info("Browser Launched Successfully");

	}

}
