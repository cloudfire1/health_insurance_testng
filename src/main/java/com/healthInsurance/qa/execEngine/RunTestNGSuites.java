package com.healthInsurance.qa.execEngine;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

import org.testng.TestNG;

public class RunTestNGSuites {

	public static void main(String[] args) {
		TestNG runner = new TestNG();

		List<String> suiteFiles = new LinkedList<String>();
		suiteFiles.add(System.getProperty("user.dir") + File.separator + "src\\main\\resources\\Suites" + File.separator
				+ "MainSuite.xml");

		runner.setTestSuites(suiteFiles);

		runner.run();

	}

}
