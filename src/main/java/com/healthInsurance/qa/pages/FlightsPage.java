package com.healthInsurance.qa.pages;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.healthInsurance.qa.base.TestBase;
import com.healthInsurance.qa.util.Log;

public class FlightsPage extends TestBase {

	@FindBy(xpath = ".//input[@id='onewayTrip']//parent::div/label")
	public WebElement OneWay;

//	@FindAll({ @FindBy(how = How.ID, using = "multiTrip"), @FindBy(name = "tripType") })
	public WebElement MultiCity;

//	@FindAll({ @FindBy(how = How.ID, using = "roundTrip"), @FindBy(name = "tripType") })
	public WebElement RoundTrip;

	@FindBy(how = How.CSS, using = "div.widget__input.is-active>a>svg")
	public WebElement suggestionBoxClearIcon;

	@FindBy(id = "from0")
	public WebElement fromWhere;

	@FindBy(id = "to0")
	public WebElement toWhere;

	@FindBy(id = "cal0")
	public WebElement DepatureDate;

	@FindBy(how = How.ID, using = "searchNow")
	public WebElement SearchNow;

	@FindBy(xpath = ".//label[contains(text(),'One Way')]")
	public WebElement oneWayLbl;

	@FindBy(id = "travellerButton")
	public WebElement travellerBtn;

	@FindBy(id = "searchNow")
	public WebElement searchFlights;

	@FindBy(how = How.XPATH, using = ".//*[@id='from0']//parent::div//section//ul/li")
	public List<WebElement> depatureFlightsSuggestion;

	@FindBy(how = How.XPATH, using = ".//*[@id='to0']//parent::div//section//ul/li")
	public List<WebElement> returnFlightsSuggestion;

	public FlightsPage() {
		super();
		PageFactory.initElements(driver, this);
	}

	public boolean verifyFlightsOneTripLabel() {
		return oneWayLbl.isDisplayed();
	}

	public void selectOneWayTripOption() {
		OneWay.click();
	}

	public void enterDepatureCity() {
		try {

			suggestionBoxClearIcon.click();

			fromWhere.sendKeys(prop.getProperty("FROM", "NONE"));

			fromWhere.sendKeys(Keys.ARROW_DOWN, Keys.ENTER);

		} catch (Exception e) {
			Log.error("Class FlightsPage | Method enterDepatureCity | Exception:::desc::" + e.getMessage());
		}
	}

	public void enterReturnCity() {
		try {
// need to check the suggestion clear box
			toWhere.sendKeys(prop.getProperty("TO", "NONE"));

			toWhere.sendKeys(Keys.TAB, Keys.TAB);

		} catch (Exception e) {
			Log.error("Class FlightsPage | Method enterReturnCity | Exception:::desc::" + e.getMessage());
		}
	}

	// May 2024
	public void datePicker(boolean monthYearNotFound, String month_year, String day, WebDriver driver) {

		outer: while (monthYearNotFound) {
			List<WebElement> headings = driver.findElements(
					By.xpath(".//div[@class='calendar__single-month active']/div[@class='calendar__month']"));

			if ((!headings.get(0).getText().trim().equalsIgnoreCase(month_year))
					&& (!headings.get(1).getText().trim().equalsIgnoreCase(month_year))) {
				driver.findElement(By.xpath(".//div[@id='widgetcalendar']//nav/a[2]")).click();

			} else {
				break outer;
			}

		}

		List<WebElement> day_lst = driver.findElements(
				By.xpath(".//div[@class='calendar__single-month active']/div[text()='" + month_year + "']//..//a"));

		WebElement mon_year = driver.findElement(
				By.xpath(".//div[@class='calendar__single-month active']/div[text()='" + month_year + "']"));

		dayLbl: for (WebElement cell : day_lst) {
			if (cell.getText().trim().equals(day) && (mon_year.getText().trim().equalsIgnoreCase(month_year))) {
				try {
					cell.click();
				} catch (ElementClickInterceptedException e) {
					e.printStackTrace();
				} catch (StaleElementReferenceException e) {
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}

				break dayLbl;
			}
		}

	}

	// Add Traveller

	public static final String traveler_Person = ".//button[@id='%s']";

	public String changeTravelerOption(String TravelerOption) { // addChild, addInfant, addSenior
		return String.format(traveler_Person, TravelerOption);

	}

	// 10
	public void addOrRemoveTraveler(String TravelerOptions, int TravelerCount) {
		try {

			travellerBtn.click();

			for (int TravelerIndex = 0; TravelerIndex < TravelerCount; TravelerIndex++) {
				WebElement travelOpt = driver.findElement(By.xpath(changeTravelerOption(TravelerOptions)));
				travelOpt.click();
			}

		} catch (Exception e) {
			e.getMessage();
		}
	}

	public void searchFlights() {
		try {
			searchFlights.click();
		} catch (Exception e) {
			e.getMessage();
		}
	}

	public Map<String, Integer> depatureAndReturnFlightsCount() {

		Map<String, Integer> depature_return_flights_cnt = new HashMap<String, Integer>();

		try {

			if (depatureFlightsSuggestion.size() < 1)
				throw new Exception("No Depature flights available for the particular state");

			if (returnFlightsSuggestion.size() < 1)
				throw new Exception("No Return flights available for the particular state");

			depature_return_flights_cnt.put("depature", depatureFlightsSuggestion.size());
			depature_return_flights_cnt.put("return", returnFlightsSuggestion.size());

		} catch (Exception e) {
			e.getMessage();
		}

		return depature_return_flights_cnt;

	}

}
