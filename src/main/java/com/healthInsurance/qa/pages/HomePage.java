package com.healthInsurance.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.healthInsurance.qa.base.TestBase;

public class HomePage extends TestBase {

	@CacheLookup
	@FindBy(xpath = "(.//div[@class='container']//child::ul/li//*[text()='cloudfir...'])[2]")
	public WebElement Login_Lbl;

	@CacheLookup
	@FindBy(how = How.XPATH, using = ".//div[@class='container']//child::ul//following::li/a[@href='/cheap-flights']")
	public WebElement FlightsLnk;

	@CacheLookup
	@FindBy(how = How.XPATH, using = ".//div[@class='container']//child::ul//following::li/a[@href='/hotels']")
	public WebElement HotelsLnk;

	@CacheLookup
	@FindBy(how = How.XPATH, using = ".//div[@class='container']//child::ul//following::li/a[@href='/car-rentals']")
	public WebElement CarsLnk;

	@CacheLookup
	@FindBy(how = How.XPATH, using = ".//div[@class='container']//child::ul//following::li/a[@href='/vacations']")
	public WebElement PackagesLnk;

	public HomePage() {
		PageFactory.initElements(driver, this);
	}

	public String VerifyHomePageTitle() {
		return driver.getTitle();

	}

	public boolean VerifyLoginLbl() {
		return Login_Lbl.isDisplayed();
	}

	public FlightsPage clickOnFlightsLnk() {
		FlightsLnk.click();
		return new FlightsPage();

	}

	public HotelsPage clickOnHotelsPage() {
		HotelsLnk.click();
		return new HotelsPage();
	}

	public CarsPage clickOnCarsPage() {
		CarsLnk.click();
		return new CarsPage();

	}

}
