package com.healthInsurance.qa.pages;

import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.healthInsurance.qa.Config.Constants;
import com.healthInsurance.qa.base.TestBase;
import com.healthInsurance.qa.util.Log;
import com.healthInsurance.qa.util.Utilities;

public class SignInPage extends TestBase {

	// private static Logger Log = Logger.getLogger(SignInPage.class);

	@CacheLookup
//	@FindBy(how = How.XPATH, using = ".//div[@id='app']//header/div[1]/div[1]/div[1]/div[2]/button")

	@FindBy(how = How.XPATH, using = ".//*[@id='app']/header/div/div[1]/div[2]/button[1]")
	public WebElement SignIn_lnk;

	@CacheLookup
//	@FindBy(xpath = "//*[@id=\"modal\"]/div/div/div/div/div/div[2]/div[2]/div[3]/div[1]/input")
	@FindBy(xpath = ".//*[@id='modal']/div/div/div/div/div/div[2]/div[2]/div[3]/div[1]/input")
	public WebElement email;

	@CacheLookup
	@FindBy(how = How.XPATH, using = ".//button[contains(text(),'Next')]")
	public WebElement Next;

	@CacheLookup
	@FindBy(name = "password")
	public WebElement password;

	@CacheLookup
//	@FindBy(xpath = ".//button[contains(text(),'Sign In')]")
	@FindBy(xpath = "(.//button[contains(text(),'Sign In')])[2]")
	public WebElement SignIn_btn;

	@CacheLookup
	@FindBy(css = ".login-popup >div>a")
	public WebElement loginPopup;

	@CacheLookup
	@FindBy(how = How.XPATH, using = ".//*[@id='app']/header/div/div/div[1]/div[1]/a")
	// @FindBy(how = How.XPATH, using
	// =".//header[@id='header']//child::div[1]/div[1]/a")
	public WebElement oneTravelLogo;

	@CacheLookup
	//@FindBy(how = How.XPATH, using = ".//span[text()='Hi, cloudfir...']")
	 @FindBy(how = How.XPATH, using = "//*[@id='user-login-button']/span[2]/span[2]")
	public WebElement loginLabel;

	@CacheLookup
	@FindBy(how = How.XPATH, using = "//*[@id='user-detail-menu']/div[3]/ul/li[7]")
//	@FindBy(how = How.XPATH, using = "//ul[2]/li[2]/div/div/ul/li[6]/span")
	public WebElement oneTravelSignoutLnk;

	@CacheLookup
	@FindBy(how = How.XPATH, using = ".//div[contains(text(),'The email or password you entered is incorrect.')]")
	public WebElement invalidEmailAddressPassword;

	/*
	 * 
	 * Initialize The Page objects present in the FindBy Annotation
	 * 
	 * 
	 */

	Utilities util = null;

	public SignInPage() {
		PropertyConfigurator.configure(Constants.LOG4J_DIR_PATH);
		PageFactory.initElements(driver, this);
		Log.info("Intialize all the webelements in the driver .............");

		util = new Utilities(driver);

	}

	public String validateSignInPageTitle() {
		Log.info("Validate the Login Page Title.....");
		return driver.getTitle();

	}

	public boolean validateOneTravelLogo() {
		Log.info("Validate the one travel Logo.....");

		Utilities.waitForElementVisible(oneTravelLogo, Constants.MAX_TIMEOUT, Constants.POLLING_TIMEOUT);

		Log.info("One Travel Logo Image is displayed.....");
		return oneTravelLogo.isDisplayed();

	}

	public HomePage SignIn(String emailAdd, String pwd) {
		Log.info("Validate the Login Page with Valid credentials....");
		try {

			Utilities.waitForElementToBeClickable(SignIn_lnk, Constants.MAX_TIMEOUT, Constants.POLLING_TIMEOUT);
			SignIn_lnk.click();

			Utilities.waitForElementVisible(email, Constants.MAX_TIMEOUT, Constants.POLLING_TIMEOUT);
			email.clear();
			email.sendKeys(emailAdd, Keys.TAB, Keys.TAB);
			Utilities.waitForElementToBeClickable(Next, Constants.MAX_TIMEOUT, Constants.POLLING_TIMEOUT);
			Next.click();

			Utilities.waitForElementVisible(password, Constants.MAX_TIMEOUT, Constants.POLLING_TIMEOUT);
			password.clear();
			password.sendKeys(pwd);

			Utilities.waitForElementToBeClickable(SignIn_btn, Constants.MAX_TIMEOUT, Constants.POLLING_TIMEOUT);
			SignIn_btn.click();

			Utilities.waitForElementToBeClickable(loginPopup, Constants.MAX_TIMEOUT, Constants.POLLING_TIMEOUT);
			loginPopup.click();

		} catch (StaleElementReferenceException e) {
			Log.error("Class SignInPage | Method SignIn | StaleElementReferenceException:::desc::" + e.getMessage());
		} catch (ElementClickInterceptedException e) {
			Log.error("Class SignInPage | Method SignIn | ElementClickInterceptedException:::desc::" + e.getMessage());
		} catch (Exception e) {
			Log.error("Class SignInPage | Method SignIn | Exception:::desc::" + e.getMessage());
		}
		return new HomePage();
	}

	public boolean validateEmail(String emailAdd, String pwd) {

		boolean bresult = false;

		try {

			Utilities.waitForElementToBeClickable(SignIn_lnk, Constants.MAX_TIMEOUT, Constants.POLLING_TIMEOUT);
			SignIn_lnk.click();

			Utilities.waitForElementVisible(email, Constants.MAX_TIMEOUT, Constants.POLLING_TIMEOUT);
			email.clear();
			email.sendKeys(emailAdd, Keys.TAB, Keys.TAB);

			Utilities.waitForElementToBeClickable(Next, Constants.MAX_TIMEOUT, Constants.POLLING_TIMEOUT);
			Next.click();

			if (!password.isDisplayed()) {

			} else {
				Utilities.waitForElementVisible(password, Constants.MAX_TIMEOUT, Constants.POLLING_TIMEOUT);
				password.clear();
				password.sendKeys(pwd);
				Utilities.waitForElementToBeClickable(SignIn_btn, Constants.MAX_TIMEOUT, Constants.POLLING_TIMEOUT);
				SignIn_btn.click();

				Utilities.waitForElementToBeClickable(loginPopup, Constants.MAX_TIMEOUT, Constants.POLLING_TIMEOUT);
				loginPopup.click();

				Utilities.waitForElementToBeClickable(loginLabel, Constants.MAX_TIMEOUT, Constants.POLLING_TIMEOUT);
				loginLabel.click();

				Utilities.waitForElementToBeClickable(oneTravelSignoutLnk, Constants.MAX_TIMEOUT,
						Constants.POLLING_TIMEOUT);
				oneTravelSignoutLnk.click();

				bresult = true;

			}

		} catch (Exception e) {
			Log.error("Class SignInPage | Method validateEmail | Exception:::desc::" + e.getMessage());
		}
		return bresult;

	}

	public boolean validatePassword(String emailAdd, String pwd) {
		boolean bvalue = false;
		try {
			Utilities.waitForElementToBeClickable(SignIn_lnk, Constants.MAX_TIMEOUT, Constants.POLLING_TIMEOUT);
			SignIn_lnk.click();

			Utilities.waitForElementVisible(email, Constants.MAX_TIMEOUT, Constants.POLLING_TIMEOUT);
			email.clear();
			email.sendKeys(emailAdd, Keys.TAB, Keys.TAB);

			Utilities.waitForElementToBeClickable(Next, Constants.MAX_TIMEOUT, Constants.POLLING_TIMEOUT);
			Next.click();

			Utilities.waitForElementVisible(password, Constants.MAX_TIMEOUT, Constants.POLLING_TIMEOUT);
			password.clear();
			password.sendKeys(pwd);
			Utilities.waitForElementToBeClickable(SignIn_btn, Constants.MAX_TIMEOUT, Constants.POLLING_TIMEOUT);
			SignIn_btn.click();

			if (!invalidEmailAddressPassword.isDisplayed()) {

			} else {
				bvalue = false;
			}

		} catch (Exception e) {
			Log.error("Class SignInPage | Method validatePassword | Exception:::desc::" + e.getMessage());

			util.refreshPage();

			Utilities.waitForElementToBeClickable(loginLabel, Constants.MAX_TIMEOUT, Constants.POLLING_TIMEOUT);
			loginLabel.click();

			Utilities.waitForElementToBeClickable(oneTravelSignoutLnk, Constants.MAX_TIMEOUT,
					Constants.POLLING_TIMEOUT);
			oneTravelSignoutLnk.click();

			bvalue = true;

		}
		return bvalue;
	}

	public void signOut() {
		try {

			Utilities.waitForElementToBeClickable(loginLabel, Constants.MAX_TIMEOUT, Constants.POLLING_TIMEOUT);
			loginLabel.click();

			Utilities.waitForElementToBeClickable(oneTravelSignoutLnk, Constants.MAX_TIMEOUT,
					Constants.POLLING_TIMEOUT);
			oneTravelSignoutLnk.click();

		} catch (Exception e) {
			Log.error("Class SignInPage | Method signOut | Exception:::desc::" + e.getMessage());
		}
	}

}
