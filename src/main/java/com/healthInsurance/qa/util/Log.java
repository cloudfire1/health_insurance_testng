package com.healthInsurance.qa.util;

import org.apache.log4j.Logger;

public class Log {

	private static Logger log = Logger.getLogger(Log.class.getName());

	/*
	 * this is to print log for the beginning of the testcases as we usally run so
	 * many testcases as a suite
	 */
	
	public static void startTestCases(String sTestCaseName) {
		log.info("*********************************************************************************************");
		log.info("*********************************************************************************************");
		log.info("*********************************************************************************************");
		log.info("*********************************************************************************************");
		log.info("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$"+sTestCaseName+"$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
		log.info("*********************************************************************************************");
		log.info("*********************************************************************************************");
		log.info("*********************************************************************************************");
		log.info("*********************************************************************************************");
		
	}
	
	
	/*
	 * this is to print log for the ending of the testcases as we usally run so
	 * many testcases as a suite
	 */
	
	public static void endTestCases(String sTestCaseName) {
		log.info("*********************************************************************************************");
		log.info("*********************************************************************************************");
		log.info("*********************************************************************************************");
		log.info("******************************E-N-D----OF-----TESTCASES------***************************************************************");
		log.info("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"+sTestCaseName+"%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
		log.info("*********************************************************************************************");
		log.info("*********************************************************************************************");
		log.info("*********************************************************************************************");
		log.info("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
		log.info("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
		log.info("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
		
		
		
	}
	
	
	/* ........Created log methods so that they can be called on various log levels(info,warm,fatal,debug,error).......................*/
	
	
	public static void info(String message) {
		
		try {
			log.info(message);
		} catch (Exception e) {
			e.getMessage();
		}
		
	}
	
	public static void warn(String message) {
		
		try {
			log.warn(message);
		} catch (Exception e) {
			e.getMessage();
		}
		
	}
	
	public static void error(String message) {
		
		try {
			log.error(message);
		} catch (Exception e) {
			e.getMessage();
		}
		
	}
	
	public static void fatal(String message) {
		
		try {
			log.fatal(message);
		} catch (Exception e) {
			e.getMessage();
		}
		
	}
	
	public static void debug(String message) {
		
		try {
			log.debug(message);
		} catch (Exception e) {
			e.getMessage();
		}
		
	}
	
	
	
	
	
	
	
	
	
	
	

}
