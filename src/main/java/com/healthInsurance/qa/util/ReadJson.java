package com.healthInsurance.qa.util;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.healthInsurance.qa.Config.Constants;

public class ReadJson {

	public String readJsonTestData(String ONT_TYPE, String key) throws IOException, ParseException {
		String json_value = null;
		JSONParser jsonParser = new JSONParser();
		try {
			FileReader fileReader = new FileReader(new File(Constants.TESTDATA_FILE_PATH));

			Object obj = jsonParser.parse(fileReader);

			JSONObject jsonobj = (JSONObject) obj;

			JSONArray array = (JSONArray) jsonobj.get(ONT_TYPE);

			JSONObject cred = (JSONObject) array.get(0);

			json_value = (String) cred.get(key);

		} catch (Exception e) {
			Log.error("Class ReadJson | Method readJsonTestData | Exception ::desc::::" + e.getMessage());
		}

		return json_value;

	}

}
