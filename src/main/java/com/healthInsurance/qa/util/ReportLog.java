package com.healthInsurance.qa.util;

import java.io.File;

import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.ExtentSparkReporterConfig;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.healthInsurance.qa.Config.Constants;
import com.healthInsurance.qa.base.TestBase;

public class ReportLog extends TestBase {

	public WebDriver driver = null;
	public ExtentReports extent = null;
	public ExtentSparkReporter spark = null;
	public ExtentTest test = null;

	public ReportLog(WebDriver driver, String timestamp, String reportName, String doc_title, String hostname,
			String env, String uname) {
		this.driver = driver;
		this.extent = new ExtentReports();
		this.spark = new ExtentSparkReporter(new File(Constants.REPORT_DIR) + File.separator+ "sparkReports_" + timestamp + ".html");
		this.spark.config(ExtentSparkReporterConfig.builder().theme(Theme.DARK).documentTitle(doc_title)
				.reportName(reportName).build());
		this.extent.setSystemInfo("Host Name", hostname);
		this.extent.setSystemInfo("Environment", env);
		this.extent.setSystemInfo("User Name", uname);
		this.extent.attachReporter(spark);
	}

	// pre-conditions for reports gen
	public void startTest(String TestName, String authorName, String category, String deviceName, String nodeName) {
		test = extent.createTest(TestName);
		test.createNode(nodeName);
		test.assignAuthor(authorName);
		test.assignCategory(category);
		test.assignDevice(deviceName);
		test.log(Status.INFO, "::::::::::::::Start of the Test Case:::::::::::::::::" + TestName);

	}

	public void logPass(String checkpointDetails) {

		String snapshotFilePath = Utilities.getScreenshot(driver);

		test.pass(checkpointDetails, MediaEntityBuilder.createScreenCaptureFromPath(snapshotFilePath).build());

	}

	public void logFail(String checkpointDetails) {

		String snapshotFilePath = Utilities.getScreenshot(driver);

		test.fail(checkpointDetails, MediaEntityBuilder.createScreenCaptureFromPath(snapshotFilePath).build());

	}

	public void logInfo(String checkpointDetails) {
		test.info(checkpointDetails);
	}

	public void logWarn(String checkPointDetails) {
		test.warning(checkPointDetails);
	}

	public void logSkip(String checkpointDetails) {
		test.skip(checkpointDetails);
	}

	// post-conditions

	public void endTest() {
		test.log(Status.INFO, ":::::End of Test case::::::::");

	}

	public void endTestSuite() {
		extent.flush();
	}

}
