package com.healthInsurance.qa.util;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public class Retry implements IRetryAnalyzer {

	int counter = 0, attempt = 5;

	/**
	 * This Below method decides how many times test should run.
	 *  TestNG call this
	 * method everytime a test is failed this method it will return true. 
	 * Everytime a test is failed needs to be retry if u get a false condition
	 */

	@Override
	public boolean retry(ITestResult result) {
		if (!result.isSuccess()) {
			if (counter < attempt) {
				counter++;
				result.setStatus(ITestResult.FAILURE);

				return true;
			} else {
				result.setStatus(ITestResult.FAILURE);
			}

		} else {
			result.setStatus(ITestResult.SUCCESS);
		}
		return false;
	}

}
