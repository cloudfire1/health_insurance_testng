package com.healthInsurance.qa.util;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.InvalidSelectorException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.io.FileHandler;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;

import com.healthInsurance.qa.Config.Constants;

public class Utilities {

	public static WebDriver driver = null;
	public static Wait<WebDriver> wait = null;
	public static JavascriptExecutor js = null;
	public static Robot robot = null;

	public static HttpURLConnection huc = null;

	public static Alert alert = null;

	public static String tot_alphabets = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "abcdefghijklmnopqrstuvwxyz";

	public static String tot_alphanumeric = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "abcdefghijklmnopqrstuvwxyz" + "0123456789";

	public Utilities(WebDriver driver) {

		this.driver = driver;

	}

	public static String timestamp() {
		Instant ins = null;
		try {
			ins = Instant.now();
		} catch (Exception e) {
			e.getMessage();
		}

		String ts = ins.toString().replace("-", "_").replace(":", "_").replace(".", "");
		return ts;

	}

	public static String getScreenshot(WebDriver driver) {

		String screenshotFilePath = null;

		screenshotFilePath = Constants.SCREENSHOTS_DIR + Utilities.timestamp() + ".png";

		File destFilePath = new File(screenshotFilePath);

		File scrFilePath = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		try {
			FileHandler.copy(scrFilePath, destFilePath);
		} catch (IOException e) {
			e.getMessage();
		}
		return screenshotFilePath;

	}

	public static String captureScreenshot(WebDriver driver, String scrshotName) {

		String screenshotFilePath = null;

		screenshotFilePath = Constants.SCREENSHOTS_DIR + scrshotName + "_" + Utilities.timestamp() + ".png";

		File destFilePath = new File(screenshotFilePath);

		File scrFilePath = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		try {
			FileHandler.copy(scrFilePath, destFilePath);
		} catch (IOException e) {
			e.getMessage();
		}
		return screenshotFilePath;

	}

	public static void leftCLickMouse() {
		Log.info("LeftCLickMouse Action is started .....................");
		try {
			robot = new Robot();
			robot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
			robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);

		} catch (Exception e) {
			Log.error("Class Utilities | Method LeftCLickMouse | Exception ::desc::::" + e.getMessage());
		}
	}

	public static void rightCLickMouse() {
		Log.info("RightCLickMouse Action is started .....................");
		try {
			robot = new Robot();
			robot.mousePress(InputEvent.BUTTON3_DOWN_MASK);
			robot.mouseRelease(InputEvent.BUTTON3_DOWN_MASK);

		} catch (Exception e) {
			Log.error("Class Utilities | Method RightCLickMouse | Exception ::desc::::" + e.getMessage());
		}
	}

	public static void middleCLickMouse() {
		Log.info("MiddleCLickMouse Action is started .....................");
		try {
			robot = new Robot();
			robot.mousePress(InputEvent.BUTTON2_DOWN_MASK);
			robot.mouseRelease(InputEvent.BUTTON2_DOWN_MASK);

		} catch (Exception e) {
			Log.error("Class Utilities | Method MiddleCLickMouse | Exception ::desc::::" + e.getMessage());
		}
	}

	public static void mouseMove(int xCor, int ycor) {
		Log.info("mouseMove Action is started .....................");

		try {
			robot = new Robot();
			robot.mouseMove(xCor, ycor);
		} catch (AWTException e) {
			Log.error("Class Utilities | Method mouseMove | AWTException ::desc::::" + e.getMessage());
		} catch (Exception e) {
			Log.error("Class Utilities | Method mouseMove | Exception ::desc::::" + e.getMessage());
		}

	}

	public static void keyPressCtrl() {
		Log.info("keyPressCtrl Action is started .....................");
		try {
			robot = new Robot();
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.delay(Constants.KEY_STROKES);
			robot.keyRelease(KeyEvent.VK_CONTROL);
		} catch (AWTException e) {
			Log.error("Class Utilities | Method keyPressCtrl | AWTException ::desc::::" + e.getMessage());
		} catch (Exception e) {
			Log.error("Class Utilities | Method keyPressCtrl | Exception ::desc::::" + e.getMessage());
		}

	}

	public static void keyPressV() {
		Log.info("keyPressV Action is started .....................");
		try {
			robot = new Robot();

			robot.keyPress(KeyEvent.VK_V);
			robot.delay(Constants.KEY_STROKES);
			robot.keyRelease(KeyEvent.VK_V);

		} catch (AWTException e) {
			Log.error("Class Utilities | Method keyPressV | AWTException ::desc::::" + e.getMessage());
		} catch (Exception e) {
			Log.error("Class Utilities | Method keyPressV | Exception ::desc::::" + e.getMessage());
		}

	}

	public static void robotUtility(String events, int index) {

		Log.info("Robot Utility Method Action is started.........");

		// "leftCLickMouse,RightCLickMouse,middleCLickMouse,keyPressCtrl,keyPressV"
		String[] eventNames = events.split(",");

		List<String> key = new LinkedList<String>();

		for (String eName : eventNames) {
			key.add(eName);
		}

		try {

			switch (key.get(index)) {
			case "leftCLickMouse":

				leftCLickMouse();

				break;

			case "rightCLickMouse":

				rightCLickMouse();

				break;

			case "middleCLickMouse":

				middleCLickMouse();

				break;

			case "keyPressCtrl":

				keyPressCtrl();

				break;

			case "keyPressV":

				keyPressV();

				break;

			default:
				throw new RuntimeException("No match case found");
			}

		} catch (Exception e) {
			Log.error("Class Utilities | Method robotUtility | Exception ::desc::::" + e.getMessage());
		}

	}

	/* ZoomIn based on levels using javascript executor */

	public static void zoom(int level) {
		Log.info("zoom Method Action is started.........");
		try {
			js = (JavascriptExecutor) driver;
			js.executeScript("document.body.style.zoom=" + level + "%", "");
		} catch (Exception e) {
			Log.error("Class Utilities | Method zoom | Exception ::desc::::" + e.getMessage());
		}
	}

	public static void selectUsingVisibleText(String txt, WebElement ele) {
		Log.info("selectUsingVisibleText Method Action is started.........");

		try {
			Select sel = new Select(ele);
			sel.selectByVisibleText(txt);
			Log.info("Locator================>" + ele + "Visible Text=========>" + txt);
		} catch (NoSuchElementException | ElementNotInteractableException | StaleElementReferenceException
				| InvalidSelectorException e) {
			Log.error(
					"Class Utilities | Method selectUsingVisibleText | NoSuchElementException | ElementNotInteractableException |StaleElementReferenceException | InvalidSelectorException ::desc::::"
							+ e.getMessage());
		} catch (Exception e) {
			Log.error("Class Utilities | Method selectUsingVisibleText | Exception ::desc::::" + e.getMessage());
		}
	}

	public static void selectUsingIndex(int index, WebElement ele) {
		Log.info("selectUsingIndex Method Action is started.........");

		try {
			Select sel = new Select(ele);
			sel.selectByIndex(index);
			Log.info("Locator================>" + ele + "Index value=========>" + index);
		} catch (NoSuchElementException | ElementNotInteractableException | StaleElementReferenceException
				| InvalidSelectorException e) {
			Log.error(
					"Class Utilities | Method selectUsingIndex | NoSuchElementException | ElementNotInteractableException |StaleElementReferenceException | InvalidSelectorException ::desc::::"
							+ e.getMessage());
		} catch (Exception e) {
			Log.error("Class Utilities | Method selectUsingIndex | Exception ::desc::::" + e.getMessage());
		}
	}

	public static void selectUsingValue(String value, WebElement ele) {
		Log.info("selectUsingValue Method Action is started.........");

		try {
			Select sel = new Select(ele);
			sel.selectByValue(value);
			Log.info("Locator================>" + ele + "selected value is=========>" + value);
		} catch (NoSuchElementException | ElementNotInteractableException | StaleElementReferenceException
				| InvalidSelectorException e) {
			Log.error(
					"Class Utilities | Method selectUsingValue | NoSuchElementException | ElementNotInteractableException |StaleElementReferenceException | InvalidSelectorException ::desc::::"
							+ e.getMessage());
		} catch (Exception e) {
			Log.error("Class Utilities | Method selectUsingValue | Exception ::desc::::" + e.getMessage());
		}
	}

	public static String getFirstSeletedValue(WebElement ele) {
		Log.info("getFirstSeletedValue Method Action is started.........");
		String firstOption = "";

		try {
			Select sel = new Select(ele);

			firstOption = sel.getFirstSelectedOption().getText();

			Log.info("Locator is :::::::::::::::::::::" + ele);

		} catch (Exception e) {
			Log.error("Class Utilities | Method getFirstSeletedValue | Exception ::desc::::" + e.getMessage());
		}
		return firstOption;
	}

	public static List<String> getAllDropDownValues(WebElement element) {
		Log.info("getAllDropDownValues Method Action is started.........");
		List<String> valueslst = null;
		try {

			Select sel = new Select(element);
			List<WebElement> element_lst = sel.getOptions();

			valueslst = new ArrayList<String>();

			for (WebElement ele : element_lst) {
				valueslst.add(ele.getText());
			}

		} catch (NoSuchElementException | ElementNotInteractableException | StaleElementReferenceException
				| InvalidSelectorException e) {
			Log.error(
					"Class Utilities | Method getAllDropDownValues | NoSuchElementException | ElementNotInteractableException |StaleElementReferenceException | InvalidSelectorException ::desc::::"
							+ e.getMessage());
		} catch (Exception e) {
			Log.error("Class Utilities | Method getAllDropDownValues | Exception ::desc::::" + e.getMessage());
		}
		return valueslst;
	}

	/* select the drop down values from the list::: if we have div tag */

	public static void selectDropDownDiv(String text, String xpath) {
		Log.info("selectDropDownDiv Method Action is started.........");

		try {
			WebElement ele = driver.findElement(By.xpath(xpath));
			ele.click();
			Thread.sleep(Constants.SYNC_TIMEOUT);
			ele.findElement(By.xpath(".//*[text()='" + text + "']")).click();

		} catch (NoSuchElementException | ElementNotInteractableException | StaleElementReferenceException
				| InvalidSelectorException e) {
			Log.error(
					"Class Utilities | Method selectDropDownDiv | NoSuchElementException | ElementNotInteractableException |StaleElementReferenceException | InvalidSelectorException ::desc::::"
							+ e.getMessage());
		} catch (Exception e) {
			Log.error("Class Utilities | Method selectDropDownDiv | Exception ::desc::::" + e.getMessage());
		}
	}

	/*
	 * Random Generation of String
	 * 
	 * 
	 * 7
	 * 
	 * 
	 */

	public static String generateRandomString(int str_len) {
		Log.info("GenerateRandomString Method Action is started.........");

		StringBuilder sb = new StringBuilder();

		try {

			for (int index = 0; index < str_len; index++) {
				int char_index = (int) (tot_alphabets.length() * Math.random()); // 12 1 10 34 43 41

				sb.append(tot_alphabets.charAt(char_index)); // LAJeyzc
			}

		} catch (Exception e) {
			Log.error("Class Utilities | Method generateRandomString | Exception ::desc::::" + e.getMessage());
		}
		return sb.toString();

	}

	/*
	 * Alpha Numeric Generation of String
	 * 
	 */

	public static String gen_Random_Alpha_Num_String(int str_len) {
		Log.info("gen_Random_Alpha_Num_String Method Action is started.........");

		StringBuilder sb = new StringBuilder();

		try {

			for (int index = 0; index < str_len; index++) {
				int char_index = (int) (tot_alphanumeric.length() * Math.random());

				sb.append(tot_alphanumeric.charAt(char_index));
			}

		} catch (Exception e) {
			Log.error("Class Utilities | Method gen_Random_Alpha_Num_String | Exception ::desc::::" + e.getMessage());
		}
		return sb.toString();

	}

	/*
	 * wait for page to Load /Get Ready
	 * document.ready_state----------------------------------> complete
	 */

	public static boolean checkPageIsReady(WebDriver driver) {

		Log.info("checkPageIsReady Method Action is started.........");

		js = (JavascriptExecutor) driver;

		try {

			String ready_state = js.executeScript("return document.ready_state", "").toString();

			if (ready_state.trim().equalsIgnoreCase("complete")) {
				return true;

			}

		} catch (Exception e) {
			Log.error("Class Utilities | Method checkPageIsReady | Exception ::desc::::" + e.getMessage());
		}
		return false;

	}

	/* Handling the Alerts */

	public static Alert getAlert() {
		Log.info("getAlert Method Action is started.........");
		try {
			alert = driver.switchTo().alert();

		} catch (Exception e) {
			Log.error("Class Utilities | Method getAlert | Exception ::desc::::" + e.getMessage());
		}
		return alert;
	}

	public static void acceptAlert() {
		Log.info("acceptAlert Method Action is started.........");

		try {
			getAlert().accept();
		} catch (Exception e) {
			Log.error("Class Utilities | Method acceptAlert | Exception ::desc::::" + e.getMessage());
		}
	}

	public static void dismissAlert() {
		Log.info("dismissAlert Method Action is started.........");

		try {
			getAlert().dismiss();
			;
		} catch (Exception e) {
			Log.error("Class Utilities | Method dismissAlert | Exception ::desc::::" + e.getMessage());
		}
	}

	public static String getAlertText() {

		Log.info("getAlertText Method Action is started.........");
		String alertText = null;
		try {
			alertText = getAlert().getText();
		} catch (Exception e) {
			Log.error("Class Utilities | Method getAlertText | Exception ::desc::::" + e.getMessage());
		}
		return alertText;

	}

	public static boolean isAlertPresent() {
		Log.info("isAlertPresent Method Action is started.........");
		try {

			getAlert();

			return true;

		} catch (Exception e) {
			Log.error("Class Utilities | Method isAlertPresent | Exception ::desc::::" + e.getMessage());

			return false;
		}
	}

	public static void acceptAlertPresent() {
		Log.info("acceptAlertPresent Method Action is started.........");

		if (!isAlertPresent()) {

			acceptAlert();

			Log.info("Alert is Accepted.........");
		}
	}

	public static void dismissAlertPresent() {
		Log.info("acceptAlertPresent Method Action is started.........");

		if (!isAlertPresent()) {

			dismissAlert();

			Log.info("Alert is Dismissed.........");
		}
	}

	public static void acceptPrompt(String txt) {

		Log.info("acceptPrompt Method Action is started.........");

		try {

			if (!isAlertPresent()) {

				alert = getAlert();
				alert.sendKeys(txt);
				alert.accept();

			}

		} catch (Exception e) {
			Log.error("Class Utilities | Method acceptPrompt | Exception ::desc::::" + e.getMessage());
		}

	}

	public static void dismissPrompt(String txt) {

		Log.info("dismissPrompt Method Action is started.........");

		try {

			if (!isAlertPresent()) {

				alert = getAlert();
				alert.sendKeys(txt);
				alert.dismiss();

			}

		} catch (Exception e) {
			Log.error("Class Utilities | Method dismissPrompt | Exception ::desc::::" + e.getMessage());
		}

	}

	public static List<Boolean> getresponsecodeUrl(String url) {

		Log.info("getresponsecodeUrl Method Action is started.........");

		boolean validResponse = false;

		List<Boolean> bvalues = new ArrayList<Boolean>();

		try {

			huc = (HttpURLConnection) new URL(url).openConnection();

			huc.setRequestMethod("HEAD");

			int responseCode = huc.getResponseCode();

			if (responseCode > 400) {
				validResponse = true;
				bvalues.add(validResponse);
				Log.info("Broken Links Url==============================>" + url);
			} else {
				validResponse = false;
			}
		} catch (MalformedURLException e) {
			Log.error(
					"Class Utilities | Method getresponsecodeUrl | MalformedURLException ::desc::::" + e.getMessage());
		} catch (IOException e) {
			Log.error("Class Utilities | Method getresponsecodeUrl | IOException ::desc::::" + e.getMessage());
		} catch (Exception e) {
			Log.error("Class Utilities | Method getresponsecodeUrl | Exception ::desc::::" + e.getMessage());
		}
		return bvalues;

	}

	/*
	 * for synchronisation of your pages we going to implement waits
	 * 
	 * 
	 */

	public static void setImplicitWait(long timeout, Duration unit) {
		Log.info("setImplicitWait Method Action is started.........");
		try {
			driver.manage().timeouts().implicitlyWait(unit == null ? Duration.ofSeconds(timeout) : unit);
		} catch (Exception e) {
			Log.error("Class Utilities | Method getresponsecodeUrl | Exception ::desc::::" + e.getMessage());
		}

	}

	public static Wait<WebDriver> getWait(int MaxTimeOutInSeconds, int PollingEveryInSeconds) {
		Log.info("getWait Method Action is started.........");
		wait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(MaxTimeOutInSeconds))
				.pollingEvery(Duration.ofSeconds(PollingEveryInSeconds)).ignoring(NoSuchElementException.class)
				.ignoring(StaleElementReferenceException.class).ignoring(ElementNotInteractableException.class)
				.ignoring(Exception.class);

		return wait;
	}

	/*
	 * wait for element visible
	 * 
	 * 
	 */

	public static void waitForElementVisible(WebElement loc, int MaxTimeOutInSeconds, int PollingEveryInSeconds) {
		Log.info("waitForElementVisible Method Action is started.........");

		Wait<WebDriver> wait = getWait(MaxTimeOutInSeconds, PollingEveryInSeconds);

		wait.until(ExpectedConditions.visibilityOf(loc));

	}

	/*
	 * wait for element to be clickable...... i have implemented only 2 conditions
	 * remaining 16 conditions u guys have to implement.........
	 * 
	 * 
	 */

	public static void waitForElementToBeClickable(WebElement loc, int MaxTimeOutInSeconds, int PollingEveryInSeconds) {
		Log.info("waitForElementToBeClickable Method Action is started.........");

		Wait<WebDriver> wait = getWait(MaxTimeOutInSeconds, PollingEveryInSeconds);

		wait.until(ExpectedConditions.elementToBeClickable(loc));

	}

	/*
	 * JavaScript Executor ........ Generic Methods
	 * 
	 * 
	 */

	public static Object executeScript(String script, Object... args) {
		Log.info("executeScript Method Action is started.........");
		try {
			js = (JavascriptExecutor) driver;

		} catch (Exception e) {
			Log.error("Class Utilities | Method executeScript | Exception ::desc::::" + e.getMessage());
		}
		return js.executeScript(script, args);
	}

	/*
	 * scroll the Element using x-cordinates and y-cordinates
	 */

	public static void scrollToElement(WebElement element) {
		Log.info("scrollToElement Method Action is started.........");
		try {
			executeScript("window.scrollTo(arguments[0],arguments[1])", element.getLocation().x,
					element.getLocation().y);

		} catch (Exception e) {
			Log.error("Class Utilities | Method scrollToElement | Exception ::desc::::" + e.getMessage());
		}
	}

	/*
	 * scroll the element and click
	 * 
	 * 
	 */

	public static void scrollToElementAndClick(WebElement element) {
		Log.info("scrollToElement Method Action is started.........");
		try {
			scrollToElement(element);
			element.click();

		} catch (Exception e) {
			Log.error("Class Utilities | Method scrollToElementAndClick | Exception ::desc::::" + e.getMessage());
		}
	}

	public static void scrollIntoView(WebElement element) {
		Log.info("scrollIntoView Method Action is started.........");
		try {

			executeScript("arguments[0].scrollIntoView();", element);

		} catch (Exception e) {
			Log.error("Class Utilities | Method scrollIntoView | Exception ::desc::::" + e.getMessage());
		}

	}

	public static void scrollIntoViewAndClick(WebElement element) {
		Log.info("scrollIntoViewAndCLick Method Action is started.........");
		try {

			executeScript("arguments[0].scrollIntoView();", element);
			element.click();

		} catch (Exception e) {
			Log.error("Class Utilities | Method scrollIntoViewAndCLick | Exception ::desc::::" + e.getMessage());
		}

	}

	public static void scrollDownVertically() {
		Log.info("scrollDownVertically Method Action is started.........");
		executeScript("window.scrollTo(0,document.body.scrollHeight)", "");
	}

	public static void scrollUpVertically() {
		Log.info("scrollDownVertically Method Action is started.........");
		executeScript("window.scrollTo(0,-document.body.scrollHeight)", "");
	}

	public static void ScrollDownByPixels(int pixelValue) {
		Log.info("ScrollDownByPixels Method Action is started.........");

		executeScript("window.scrollBy(0," + pixelValue + ")");
	}

	public static void ScrollUpByPixels(int pixelValue) {
		Log.info("ScrollUpByPixels Method Action is started.........");

		executeScript("window.scrollBy(0,-" + pixelValue + ")");
	}

	/* Generate Random Number generation */

	public static int generateRandomNumber(int max, int min) {
		Log.info("generateRandomNumber Method Action is started.........");
		int randomNumber = 0;
		try {
			randomNumber = (int) Math.floor(Math.random() * (max - min) + min);
		} catch (Exception e) {
			Log.error("Class Utilities | Method generateRandomNumber | Exception ::desc::::" + e.getMessage());
		}
		return randomNumber;
	}

	/*
	 * verify element is present
	 * 
	 * 
	 */

	public static synchronized boolean verifyElementIsPresent(WebElement element) {
		Log.info("verifyElementIsPresent Method Action is started.........");
		boolean isDisplayed = false;
		try {
			isDisplayed = element.isDisplayed();
		} catch (Exception e) {
			Log.error("Class Utilities | Method verifyElementIsPresent | Exception ::desc::::" + e.getMessage());
		}
		return isDisplayed;
	}

	public void refreshPage() {
		Log.info("refreshPage Method Action is started.........");
		try {
			driver.navigate().refresh();
		} catch (Exception e) {
			Log.error("Class Utilities | Method refreshPage | Exception ::desc::::" + e.getMessage());
		}
	}

	public static synchronized boolean verifyTextEquals(WebElement element, String expectedTxt) {
		Log.info("verifyTextEquals Method Action is started.........");

		boolean flag = false;

		try {
			String actualTxt = element.getText();
			if (expectedTxt.equals(actualTxt)) {

				Log.info("Actual Text is :::::" + actualTxt + "::::::::" + "expectedTxt:::::::" + expectedTxt
						+ ":::::::::::::");
				flag = true;
			}
		} catch (Exception e) {
			Log.error("Class Utilities | Method verifyTextEquals | Exception ::desc::::" + e.getMessage());
		}
		return flag;
	}

	public static void TimeOuts(int timeout) {

		try {
			Thread.sleep(timeout);
		} catch (InterruptedException e) {
			Log.error("Class Utilities | Method TimeOuts | InterruptedException ::desc::::" + e.getMessage());
		} catch (Exception e) {
			Log.error("Class Utilities | Method TimeOuts | Exception ::desc::::" + e.getMessage());
		}

	}

}
