package com.healthInsurance.qa.functional;

import java.io.IOException;
import java.lang.reflect.Method;

import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.healthInsurance.qa.base.TestBase;
import com.healthInsurance.qa.pages.FlightsPage;
import com.healthInsurance.qa.pages.SignInPage;
import com.healthInsurance.qa.util.Log;
import com.healthInsurance.qa.util.ReadJson;
import com.healthInsurance.qa.util.ReportLog;

public class FlightsPageTest extends TestBase {

	SignInPage signIn = null;
	FlightsPage flight = null;
	public ReportLog reports = null;

	public ReadJson json = null;

	public FlightsPageTest() {
		super();
	}

	@BeforeSuite(alwaysRun = true)
	public void setUp() {
		BrowserSetup();

		Log.startTestCases("=======================> Flights Page Test<==================================="
				+ "=========================================================================================================================="
				+ "===========================================================================================");

		long ts = System.currentTimeMillis();

		reports = new ReportLog(driver, String.valueOf(ts), "One Travel Reports", "Travel Reports for ONT Module",
				"OneTravel", "UAT", "SATHISH");
		json = new ReadJson();
		signIn = new SignInPage();
		flight = new FlightsPage();

	}

	@BeforeMethod(alwaysRun = true)
	public void oneTravelLogin(Method m) {
		signIn.SignIn(prop.getProperty("username", "NONE"), prop.getProperty("password", "NONE"));

		reports.startTest(m.getName(), "sathish", "Functional Testing", "Windows 10", "Login_Node");
	}

	@Test(priority = -3, enabled = true, suiteName = "Functional", testName = "SearchForFlights", description = "search the flights based on depature and return city", singleThreaded = true, ignoreMissingDependencies = true, groups = {
			"Functional", "Regression" })
	public void SearchForFlights()  {
		boolean monthYearNotFound = true;
		flight.selectOneWayTripOption();
		flight.enterDepatureCity();
		flight.enterReturnCity();
		int Traveller_Count = 0;
		try {
			String depature_date = json.readJsonTestData("Flights", "CheckInMonthYear");
			
			System.out.println(depature_date);
			String day = json.readJsonTestData("Flights", "CheckInDay");
			
			System.out.println(day);

			flight.datePicker(monthYearNotFound, depature_date, day, driver);
			Traveller_Count = Integer.valueOf(json.readJsonTestData("Flights", "Add_Remove_Traveller"));
		} catch (IOException | ParseException e) {
			Log.error("Class FlightsPageTest | Method SearchForFlights | Exception ::desc::::" + e.getMessage());
		}

		
		flight.addOrRemoveTraveler("addadults", Traveller_Count);
		flight.searchFlights();

		reports.logInfo("Flight is listed on the Flights Search Page");
	}

	@AfterClass
	public void tearDown() {

		reports.endTest();
	}

	public void quits() {

		if (driver != null) {
			driver.quit();
			reports.endTestSuite();
			Log.endTestCases(
					"<=========================================E--N---D of search FLights Test ==========================>");
		}

	}

}
