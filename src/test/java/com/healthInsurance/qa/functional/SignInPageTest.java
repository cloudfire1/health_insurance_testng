package com.healthInsurance.qa.functional;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Method;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


import com.healthInsurance.qa.Config.Constants;
import com.healthInsurance.qa.base.TestBase;
import com.healthInsurance.qa.pages.SignInPage;
import com.healthInsurance.qa.util.Log;
import com.healthInsurance.qa.util.ReportLog;
import com.healthInsurance.qa.util.Utilities;

public class SignInPageTest extends TestBase {

	protected SignInPage signInPage;

	public ReportLog reports = null;

	protected Utilities util = null;

	public SignInPageTest() {
		super();
	}

	@BeforeSuite(alwaysRun = true)
	public void loadConfig() {
		BrowserSetup();

		Log.startTestCases("=======================> SignIn Page Test<==================================="
				+ "=========================================================================================================================="
				+ "===========================================================================================");

		long ts = System.currentTimeMillis();

		reports = new ReportLog(driver, String.valueOf(ts), "One Travel Reports", "Travel Reports for ONT Module",
				"OneTravel", "UAT", "SATHISH");

		util = new Utilities(driver);

	}

	@BeforeMethod(alwaysRun = true)
	public void oneTravelLogin(Method m) {
		signInPage = new SignInPage();
		reports.startTest(m.getName(), "sathish", "Functional Testing", "Windows 10", "Login_Node");
	}

	@Test(priority = -1, description = "validate the SignUp Page Title", singleThreaded = true, enabled = true, ignoreMissingDependencies = true, groups = {
			"Functional", "Regression" })
	public void SignUpPageTitleTest() {
		String pageTitle = signInPage.validateSignInPageTitle();

		if (pageTitle.equals("Cheap Airline Tickets, Cheap Flights and Air Travel Deals - OneTravel")) {
			reports.logPass("Validated the SignUp Page successfully.......");
		} else {
			reports.logFail("Invalid Page Title for the SignUp Page");
		}

		Assert.assertEquals(pageTitle, "Cheap Airline Tickets, Cheap Flights and Air Travel Deals - OneTravel");

	}

	@Test(priority = 0, dependsOnMethods = {
			"SignUpPageTitleTest" }, singleThreaded = true, ignoreMissingDependencies = true, enabled = true, suiteName = "FunctionalSuite", description = "Validate the One Travel Logo", groups = {
					"Functional", "Regression" })
	public void oneTravelLogoImageTest() {
		boolean flag = signInPage.validateOneTravelLogo();
		if (flag) {
			reports.logPass("Validated the OneTravel Logo successfully.......");
		} else {
			reports.logFail("OneTravel Logo is not displayed");
		}

		Assert.assertTrue(flag);
	}

	@Test(priority = 1, dependsOnMethods = {
			"oneTravelLogoImageTest" }, enabled = true, description = "Validate the SignIn Page by providing the "
					+ "valid credentials", singleThreaded = true, groups = { "Functional", "Regression" })
	public void ValidateSignInPageTest() {
		reports.logInfo("Verify the login flow with valid credentials");

		try {

			signInPage.SignIn(prop.getProperty("username", "NONE"), prop.getProperty("password", "NONE"));

		} catch (Exception e) {
			reports.logFail("Exception has occured:::::Method =====>ValidateSignInPageTest" + e.getMessage());
		}

		signInPage.signOut();
	}

	@DataProvider(name = "ReadJsonArrayEmail")
	public String[] readJsonEmail() throws IOException, ParseException {

		JSONParser jsonParser = new JSONParser();

		FileReader fileReader = new FileReader(new File(Constants.CREDENTIALS_FILE_PATH));

		Object obj = jsonParser.parse(fileReader);

		JSONObject jsonobj = (JSONObject) obj;

		JSONArray array = (JSONArray) jsonobj.get("validEmail");

		String arr[] = new String[array.size()]; // 3

		for (int index = 0; index < array.size(); index++) {

			JSONObject cred = (JSONObject) array.get(index);

			String email_Id = (String) cred.get("Email");
			String pwd = (String) cred.get("Password");

			arr[index] = email_Id + "," + pwd;

		}

		return arr;

	}

	@DataProvider(name = "ReadJsonArrayPwd")
	public String[] readJsonPwd() throws IOException, ParseException {

		JSONParser jsonParser = new JSONParser();

		FileReader fileReader = new FileReader(new File(Constants.CREDENTIALS_FILE_PATH));

		Object obj = jsonParser.parse(fileReader);

		JSONObject jsonobj = (JSONObject) obj;

		JSONArray array = (JSONArray) jsonobj.get("validpassword");

		String arr[] = new String[array.size()]; // 3

		for (int index = 0; index < array.size(); index++) {

			JSONObject cred = (JSONObject) array.get(index);

			String email_Id = (String) cred.get("Email");
			String pwd = (String) cred.get("Password");

			arr[index] = email_Id + "," + pwd;

		}

		return arr;

	}

	@Test(priority = 2, dataProvider = "ReadJsonArrayEmail", enabled = true, testName = "ValidEmailFields", suiteName = "SMoke", singleThreaded = true, ignoreMissingDependencies = true, invocationCount = 1,
			 groups = { "Smoke", "Functional" })
	public void validEmailFields(String cred) {

		String[] user_cred = cred.split(",");

		boolean bresult = signInPage.validateEmail(user_cred[0], user_cred[1]);

		if (bresult) {
			Log.info(
					"==================================> Email Id validation is successfull==============================>");

			reports.logPass("Email Id validation is successfull" + "::::::::::emailId:::::::::" + user_cred[0]
					+ "::::password:::" + user_cred[1]);
		} else {
			reports.logFail(
					"=================================>Invalid EMail id==================================================>");

			util.refreshPage();

			Log.error(
					"=========================================>Invalid Login ==========================================>");
		}

	}

	
	/*
	 * @Test(priority = 3, dataProvider = "ReadJsonArrayPwd", enabled = true,
	 * testName = "ValidPasswordFields", suiteName = "Smoke", singleThreaded = true,
	 * ignoreMissingDependencies = true, groups = { "Smoke", "Functional" }) public
	 * void validatePasswordFields(String cred) {
	 * 
	 * String[] user_cred = cred.split(",");
	 * 
	 * boolean bresult = signInPage.validatePassword(user_cred[0], user_cred[1]);
	 * 
	 * if (bresult) { Log.info(
	 * "==================================> password validation is successfull==============================>"
	 * );
	 * 
	 * reports.logPass("password validation is successfull" +
	 * "::::::::email id::::::::::::::" + user_cred[0] + ":::::password:::" +
	 * user_cred[1]); } else { reports.logFail(
	 * "=================================>Invalid EMail id==================================================>"
	 * );
	 * 
	 * util.refreshPage();
	 * 
	 * Log.error(
	 * "=========================================>Invalid Login ==========================================>"
	 * ); }
	 * 
	 * }
	 */
	 

	@AfterMethod(alwaysRun = true)
	public void tearDown() {
		reports.endTest();
	}

	@AfterSuite(alwaysRun = true)
	public void endTestSuite() {
		driver.quit();
		reports.endTestSuite();

		Log.endTestCases("<============================== END of SignPage Test====================================>");
	}

}
