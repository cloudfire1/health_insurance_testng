package com.healthInsurance.qa.smoke;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

public class DataPicker_ibibo {

	static WebDriver driver = null;

	public static void main(String[] args) throws InterruptedException {
		boolean monthYearNotFound;

		FirefoxOptions options = new FirefoxOptions();

		options.addArguments("-incognito");
		options.addArguments("--start-maximized");

		driver = new FirefoxDriver(options);

		JavascriptExecutor js = (JavascriptExecutor) driver;

		js.executeScript("window.location='https://www.goibibo.com/flights/'");

		Thread.sleep(16000);

		driver.findElement(By.xpath("(//span[text()='Departure']//..)[1]")).click();

		// user inputs
		String month_year = "July 2024";

		String date = "24";

		monthYearNotFound = true;

		outer: while (monthYearNotFound) {
			// December 2023, January 2024
			List<WebElement> heading = driver
					.findElements(By.xpath("//div[@class='DayPicker-Month']//div[@role='heading']"));

			if ((!heading.get(0).getText().trim().equalsIgnoreCase(month_year))
					&& (!heading.get(1).getText().trim().equalsIgnoreCase(month_year))) {

				driver.findElement(By.xpath("//div[@class='DayPicker-NavBar']/span[2]")).click();

			} else {
				break outer;
				// monthYearNotFound= false;

			}

		}

		List<WebElement> date_lst = driver
				.findElements(By.xpath("//div[@class='DayPicker-Month']//div[@role='heading']/div[text()='" + month_year
						+ "']//..//..//div[@class='DayPicker-Body']/div/div/p"));

		WebElement done_btn = driver.findElement(By.xpath("//span[text()='Done']"));

		WebElement mon_yr = driver.findElement(
				By.xpath("//div[@class='DayPicker-Month']//div[@role='heading']/div[text()='" + month_year + "']"));

		out: for (WebElement cell : date_lst) {

			if (cell.getText().trim().equals(date) && (mon_yr.getText().trim().equalsIgnoreCase(month_year))) {

				try {
					cell.click();
				} catch (ElementClickInterceptedException e) {

					e.printStackTrace();
				} catch (StaleElementReferenceException e) {

					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}

				done_btn.click();
				break out;

			}

		}

	}

}
