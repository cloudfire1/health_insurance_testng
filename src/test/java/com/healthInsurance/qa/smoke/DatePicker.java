package com.healthInsurance.qa.smoke;

import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

public class DatePicker {

	static WebDriver driver = null;

	// 13
	public static void datePic(WebDriver driver, String date) {
		try {

			WebElement datePicker = driver.findElement(By.id("ui-datepicker-div"));

			List<WebElement> datepicker_lst = datePicker.findElements(By.tagName("a"));

			outer: for (WebElement cell : datepicker_lst) {
				if (cell.getText().trim().equals(date)) {
					cell.click();

					break outer;
				}
			}

		} catch (NoSuchElementException e) {
			e.printStackTrace();
		} catch (ElementClickInterceptedException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

	public static void main(String[] args) throws InterruptedException {

		FirefoxOptions option = new FirefoxOptions();
		option.addArguments("-incognito");
		option.addArguments("--start-maximized");
		driver = new FirefoxDriver(option);

		JavascriptExecutor js = (JavascriptExecutor) driver;

		js.executeScript("window.location='http://only-testing-blog.blogspot.com/2014/09/selectable.html'");

		List<String> monthList = Arrays.asList("January", "February", "March", "April", "May", "June", "July", "August",
				"September", "October", "November", "December");

		int expMonth, expYear;
		String expDate = null; // user values

		String calMonth = null;
		String calYear = null;

		boolean dateNotFound;

		Thread.sleep(5000);

		driver.findElement(By.id("datepicker")).click();

		dateNotFound = true;

		expMonth = 5;
		expYear = 2024;
		expDate = "11";
		// exp -aug-2008 cal - aug-2012
		while (dateNotFound) {

			calMonth = driver.findElement(By.className("ui-datepicker-month")).getText(); // month- november

			calYear = driver.findElement(By.className("ui-datepicker-year")).getText(); // year

			if ((monthList.indexOf(calMonth) + 1 == expMonth) && (expYear == Integer.parseInt(calYear))) {
				datePic(driver, expDate);
				dateNotFound = false;

			} else if (monthList.indexOf(calMonth) + 1 < expMonth && (expYear == Integer.parseInt(calYear))
					|| (expYear > Integer.parseInt(calYear))) {

				driver.findElement(By.xpath("//div[@id='ui-datepicker-div']/div/a[2]/span")).click(); // next

			} else if (monthList.indexOf(calMonth) + 1 > expMonth && (expYear == Integer.parseInt(calYear))
					|| (expYear < Integer.parseInt(calYear))) {
				driver.findElement(By.xpath("//div[@id='ui-datepicker-div']/div/a[1]/span")).click(); // previous
			}
		}

	}

}
